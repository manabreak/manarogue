extern crate rand;

use super::{SCR_WIDTH, SCR_HEIGHT, W_WIDTH, W_HEIGHT};

use std::cmp;
use self::rand::Rng;

const EMPTY: char = ' ';
const FLOOR: char = '.';
const WALL: char = '#';

pub struct World {
    world: [char; (W_WIDTH * W_HEIGHT) as usize]
}

impl World {
    pub fn new() -> World {
        World {
            world: [' '; (W_WIDTH * W_HEIGHT) as usize]
        }
    }

    pub fn gen(&mut self) {
        let room_left: u32 = rand::thread_rng().next_u32() % 10;
        let room_top: u32 = rand::thread_rng().next_u32() % 10;
        let room_right: u32 = cmp::min(15 + rand::thread_rng().next_u32() % 10, W_WIDTH as u32);
        let room_bottom: u32 = cmp::min(12 + rand::thread_rng().next_u32() % 10, W_HEIGHT as u32);

        for y in (room_top)..(room_bottom) {
            for x in (room_left)..(room_right) {
                self.world[(y * W_WIDTH as u32 + x) as usize] = FLOOR;
            }
        }

        for e in (room_top)..(room_bottom) {
            self.world[(e * W_WIDTH as u32 + room_left) as usize] = WALL;
            self.world[(e * W_WIDTH as u32 + room_right) as usize] = WALL;
        }

        for e in (room_left)..(room_right + 1) {
            self.world[(room_top * W_WIDTH as u32 + e) as usize] = WALL;
            self.world[(room_bottom * W_WIDTH as u32 + e) as usize] = WALL;
        }
    }

    pub fn at(&self, x: i32, y: i32) -> char {
        self.world[(y * W_WIDTH + x) as usize]
    }

    pub fn is_free(&self, x: i32, y: i32 ) -> bool {
        self.at(x, y) == FLOOR
    }

    pub fn can_traverse(&self, x: i32, y: i32) -> bool {
        let c = self.at(x, y);
        c == FLOOR
    }
}
