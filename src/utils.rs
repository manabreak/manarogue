pub struct Rect {
    x: u32,
    y: u32,
    width: u32,
    height: u32
}

pub struct RectArray<T> {
    rect: Rect,
    data: Vec<T>
}

impl<T> RectArray<T> {
    pub fn new(x: u32, y: u32, width: u32, height: u32) -> RectArray<T> {
        RectArray {
            rect: Rect::new(x, y, width, height),
            data: Vec::with_capacity((width * height) as usize)
        }
    }

    pub fn from_bounds(left: u32, top: u32, right: u32, bottom: u32) -> RectArray<T> {
        RectArray {
            rect: Rect::from_bounds(left, top, right, bottom),
            data: Vec::with_capacity(((right - left) * (bottom - top)) as usize)
        }
    }
}

impl Rect {
    pub fn new(x: u32, y: u32, width: u32, height: u32) -> Rect {
        Rect{ x, y, width, height }
    }

    pub fn from_bounds(left: u32, top: u32, right: u32, bottom: u32) -> Rect {
        Rect {
            x: left,
            y: top,
            width: (right - left),
            height: (bottom - top)
        }
    }

    pub fn x(&self) -> u32 { self.x }
    pub fn y(&self) -> u32 { self.y }
    pub fn width(&self) -> u32 { self.width }
    pub fn height(&self) -> u32 { self.height }

    pub fn left(&self) -> u32 { self.x }
    pub fn top(&self) -> u32 { self.y }
    pub fn right(&self) -> u32 { self.x + self.width }
    pub fn bottom(&self) -> u32 { self.y + self.height }

    pub fn position(&self) -> (u32, u32) { (self.x, self.y) }
    pub fn size(&self) -> (u32, u32) { (self.width, self.height) }
    pub fn is_square(&self) -> bool { self.width == self.height }

    pub fn mid(&self) -> (u32, u32) {
        (
            self.x + self.width / 2,
            self.y + self.height / 2
        )
    }

    pub fn half_size(&self) -> (u32, u32) {
        (
            self.width / 2,
            self.height / 2
        )
    }

    pub fn bounds(&self) -> (u32, u32, u32, u32) {
        (
            self.left(),
            self.top(),
            self.right(),
            self.bottom()
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_mid() {
        let r = Rect::new(3, 3, 2, 2);
        let (x, y) = r.mid();
        assert_eq!(4, x);
        assert_eq!(4, y);

        let r = Rect::new(3, 3, 3, 3);
        let (x, y) = r.mid();
        assert_eq!(4, x);
        assert_eq!(4, y);
    }

    #[test]
    fn test_position() {
        let r = Rect::new(5, 7, 3, 3);
        let (x, y) = r.position();
        assert_eq!(5, x);
        assert_eq!(7, y);
    }

    #[test]
    fn test_is_square() {
        let r = Rect::new(0, 0, 4, 4);
        assert!(r.is_square());

        let r = Rect::new(0, 0, 6, 3);
        assert!(!r.is_square());
    }

    #[test]
    fn test_size() {
        let r = Rect::new(0, 0, 5, 8);
        let (w, h) = r.size();
        assert_eq!(5, w);
        assert_eq!(8, h);
    }

    #[test]
    fn test_new() {
        let r = Rect::new(1, 2, 5, 6);
        assert_eq!(1, r.x());
        assert_eq!(2, r.y());
        assert_eq!(5, r.width());
        assert_eq!(6, r.height());

        assert_eq!(1, r.left());
        assert_eq!(2, r.top());
        assert_eq!(6, r.right());
        assert_eq!(8, r.bottom());
    }

    #[test]
    fn test_from_bounds() {
        let r = Rect::from_bounds(1, 2, 4, 4);
        assert_eq!(1, r.x());
        assert_eq!(2, r.y());
        assert_eq!(3, r.width());
        assert_eq!(2, r.height());

        assert_eq!(1, r.left());
        assert_eq!(2, r.top());
        assert_eq!(4, r.right());
        assert_eq!(4, r.bottom());
    }

    #[test]
    fn test_rectarray_ctors() {
        let a: RectArray<char> = RectArray::new(0, 0, 5, 4);
        assert_eq!(5, a.rect.width());
        assert_eq!(4, a.rect.height());

        let a: RectArray<char> = RectArray::from_bounds(1, 1, 3, 2);
        assert_eq!(2, a.rect.width());
        assert_eq!(1, a.rect.height());
    }
}
