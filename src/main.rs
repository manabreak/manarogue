extern crate pancurses;

const SCR_WIDTH: i32 = 80;
const SCR_HEIGHT: i32 = 24;
const W_WIDTH: i32 = 50;
const W_HEIGHT: i32 = 20;

mod npc;
mod player;
mod world;
mod entity;
mod core;
mod terminalcontext;
mod game;
mod utils;

use pancurses::{initscr, endwin};
use core::Context;
use terminalcontext::TerminalContext;
use game::Game;

fn main() {
    let mut game = Game::create();
    let mut context = TerminalContext::new();
    context.initialize(&game);

    loop {
        context.render(&game);
        context.update(&mut game);
    }

    context.cleanup();
}
