use game::Game;

pub trait Context {
    // TODO Make return a result
    fn initialize(&mut self, game: &Game);

    fn clear(&self);

    fn update(&self, game: &mut Game);

    /// Renders the game world
    fn render(&self, game: &Game);

    fn cleanup(&self);
}
