use entity::*;

pub struct Player {
    pub entity: Entity,
    pub name: String
}

impl Player {
    pub fn new() -> Player {
        Player {
            entity: Entity::new(10, 10, '@'),
            name: String::from("Kenny")
        }
    }
}
