pub struct Color(f32, f32, f32);

const COLOR_WHITE: Color = Color(1.0, 1.0, 1.0);
const COLOR_RED: Color = Color(1.0, 0.0, 0.0);
const COLOR_GREEN: Color = Color(0.0, 1.0, 0.0);
const COLOR_BLUE: Color = Color(0.0, 0.0, 1.0);


pub struct Entity {
    x: i32,
    y: i32,
    prev_x: i32,
    prev_y: i32,
    c: char
}

impl Entity {
    pub fn new(x: i32, y: i32, c: char) -> Entity {
        Entity {
            x,
            y,
            prev_x: x,
            prev_y: y,
            c
        }
    }

    pub fn get_x(&self) -> i32 { self.x }
    pub fn get_y(&self) -> i32 { self.y }
    pub fn get_prev_x(&self) -> i32 { self.prev_x }
    pub fn get_prev_y(&self) -> i32 { self.prev_y }

    pub fn get_pos(&self) -> (i32, i32) { (self.x, self.y) }
    pub fn get_prev_pos(&self) -> (i32, i32) { (self.prev_x, self.prev_y)}

    pub fn get_char(&self) -> char { self.c }

    pub fn set_pos(&mut self, x: i32, y: i32) {
        self.x = x;
        self.y = y;
        self.prev_x = x;
        self.prev_y = y;
    }

    pub fn set_x(&mut self, x: i32) {
        self.x = x;
        self.prev_x = x;
    }

    pub fn set_y(&mut self, y: i32) {
        self.y = y;
        self.prev_y = y;
    }

    pub fn move_left(&mut self) {
        self.set_prev();
        self.x -= 1;
    }

    pub fn move_right(&mut self) {
        self.set_prev();
        self.x += 1;
    }

    pub fn move_up(&mut self) {
        self.set_prev();
        self.y -= 1;
    }

    pub fn move_down(&mut self) {
        self.set_prev();
        self.y += 1;
    }

    fn set_prev(&mut self) {
        self.prev_x = self.x;
        self.prev_y = self.y;
    }
}
