extern crate pancurses;

use super::{SCR_WIDTH, SCR_HEIGHT, W_WIDTH, W_HEIGHT};
use pancurses::{
    initscr,
    endwin,
    noecho,
    resize_term,
    start_color,
    init_pair,
    COLOR_PAIR,
    COLORS,
    curs_set,
    Input
};
use game::Game;
use core::Context;

pub struct TerminalContext {
    window: pancurses::Window,
    w: i32,
    h: i32
}

impl Context for TerminalContext {
    fn initialize(&mut self, game: &Game) {
        noecho();
        self.window.timeout(100);
        start_color();
        println!("Colors supported: {}", COLORS());
        init_pair(1, 1, 0);
        curs_set(0);
        self.window.keypad(true);
        self.w = self.window.get_max_x();
        self.h = self.window.get_max_y();

        if self.w > W_WIDTH {
            self.w = W_WIDTH;
        }

        if self.h > W_HEIGHT {
            self.h = W_HEIGHT;
        }

        println!("Window size: {}, {}", self.w, self.h);

        for y in 0..self.h {
            self.window.mv(y, 0);
            for x in 0..self.w {
                self.window.addch(game.world_at(x, y));
            }
        }
    }

    fn clear(&self) {
        self.window.erase();
    }

    fn update(&self, game: &mut Game) {
        let mut acted = false;
        match self.window.getch() {
            Some(Input::KeyDown) => {
                game.on_move_down();
                acted = true;
            },
            Some(Input::KeyUp) => {
                game.on_move_up();
                acted = true;
            },
            Some(Input::KeyLeft) => {
                game.on_move_left();
                acted = true;
            },
            Some(Input::KeyRight) => {
                game.on_move_right();
                acted = true;
            },
            _ => ()
        }

        if acted {
            game.update();
        }
    }

    fn render(&self, game: &Game) {

        let ref player = game.get_player().entity;
        let px = player.get_x();
        let py = player.get_y();

        let old_px = player.get_prev_x();
        let old_py = player.get_prev_y();

        if old_px != px || old_py != py {
            self.window.mvaddch(old_py, old_px, game.world_at(old_px, old_py));
        }

        self.window.mvaddch(py, px, player.get_char());

        for npc in game.get_npcs() {
            let ref e = npc.entity;
            self.window.mvaddch(e.get_x(), e.get_y(), e.get_char());
        }

        self.draw_ui(game);

        self.window.refresh();
    }

    fn cleanup(&self) {
        endwin();
    }
}

impl TerminalContext {
    pub fn new() -> TerminalContext {
        let window = initscr();

        TerminalContext {
            w: 0,
            h: 0,
            window
        }
    }

    fn draw_ui(&self, game: &Game) {
        let ref player = game.get_player().entity;
        // Stat row 1
        self.window.mv(W_HEIGHT, 0);
        self.window.printw(game.get_status());
        self.window.clrtoeol();

        // Stat row 2
        self.window.mv(W_HEIGHT + 1, 0);
        self.window.printw(format!("Player at {}, {} (was previously {}, {})", player.get_x(), player.get_y(), player.get_prev_x(), player.get_prev_y()).as_str());
        self.window.clrtoeol();

        // Info row
        self.window.mv(W_HEIGHT + 2, 0);
        if game.has_info() {
            self.window.printw(game.get_info());
        }
        self.window.clrtoeol();

        // Debug row
        self.window.mv(W_HEIGHT + 3, 0);
        self.window.printw(game.get_debug());
        self.window.clrtoeol();

        self.window.clrtoeol();
    }
}
