extern crate rand;

const INFO_STR: &'static str = include_str!("data/text_en.txt");

use player::Player;
use entity::Entity;
use world::World;
use npc::Npc;

pub struct Game<'a> {
    world: World,
    player: Player,

    status: String,

    info: i32,
    info_strings: Vec<&'a str>,

    /// Step counter, ie. "how many actions player has done"
    steps: u64,
    debug: String,

    npcs: Vec<Npc>
}

impl<'a> Game<'a> {
    pub fn create() -> Game<'a> {
        let mut game = Game {
            world: World::new(),
            player: Player::new(),
            status: String::with_capacity(80),
            info: -1,
            info_strings: Vec::with_capacity(80),
            steps: 0,
            debug: String::with_capacity(80),
            npcs: Vec::with_capacity(20)
        };

        let mut lines = INFO_STR.lines();
        loop {
            match lines.next() {
                Some(line) => {
                    game.info_strings.push(&line);
                },
                None => break
            }
        }

        game.world.gen();
        game.status.push_str(format!("{} the Paladin", &game.player.name).as_str());
        game.debug.push_str("World generated.");

        game.npcs.push(Npc::new(8, 8, 'r'));

        game
    }

    fn check_entity_movement(world: &World, entity: &mut Entity) {
        let (x, y) = entity.get_pos();
        if !world.can_traverse(x, y) {
            let (x, y) = entity.get_prev_pos();
            entity.set_pos(x, y);
        }
    }

    pub fn on_move_up(&mut self) {
        self.player.entity.move_up();
    }

    pub fn on_move_down(&mut self) {
        self.player.entity.move_down();
    }

    pub fn on_move_left(&mut self) {
        self.player.entity.move_left();
    }

    pub fn on_move_right(&mut self) {
        self.player.entity.move_right();
    }

    pub fn get_player(&self) -> &Player {
        &self.player
    }

    pub fn get_npcs(&self) -> &Vec<Npc> {
        &self.npcs
    }

    pub fn on_char(&mut self, c: char) {

    }

    pub fn world_at(&self, x: i32, y: i32) -> char {
        self.world.at(x, y)
    }

    pub fn get_status(&self) -> &str {
        &self.status
    }

    pub fn has_info(&self) -> bool {
        self.info > -1
    }

    pub fn get_info(&self) -> &str {
        self.info_strings[self.info as usize]
    }

    pub fn get_debug(&self) -> &str {
        &self.debug.as_str()
    }

    pub fn update(&mut self) {
        self.steps += 1;
        Game::check_entity_movement(&self.world, &mut self.player.entity);
    }
}
