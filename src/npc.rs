use entity::Entity;

pub struct Npc {
    pub entity: Entity
}

impl Npc {
    pub fn new(x: i32, y: i32, c: char) -> Npc {
        Npc {
            entity: Entity::new(x, y, c)
        }
    }
}
